#!/bin/bash

NAME="delicious.matchama"
DIR="/usr/share/plasma/look-and-feel/$NAME/"

if sudo cp -R ./$NAME $DIR; then
    echo "Theme installed in $DIR"
else
    echo "Errors occurred during installation, exiting"
    exit;
fi
